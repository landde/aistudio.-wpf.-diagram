﻿using System;
using System.Collections.Generic;
using System.Text;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner.Demo.ViewModels
{
    public class CustomDefinedDesignerItemViewModel : DesignerItemViewModelBase
    {
        public CustomDefinedDesignerItemViewModel() : this(null)
        {

        }

        public CustomDefinedDesignerItemViewModel(IDiagramViewModel root) : base(root)
        {

        }

        public CustomDefinedDesignerItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public CustomDefinedDesignerItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        public override SelectableItemBase GetSerializableObject()
        {
            return new DesignerItemBase(this, Answer);
        }

        public override void Init(IDiagramViewModel root, bool initNew)
        {
            base.Init(root, initNew);
        }

        protected override void InitNew()
        {
            this.ItemWidth = 150;
            this.ItemHeight = 80;
            AddConnector(new FullyCreatedConnectorInfo(this, ConnectorOrientation.Top));
            AddConnector(new FullyCreatedConnectorInfo(this, ConnectorOrientation.Bottom));
        }

        protected override void LoadDesignerItemViewModel(SelectableItemBase designerbase)
        {
            base.LoadDesignerItemViewModel(designerbase);

            if (designerbase is DesignerItemBase designer)
            {
                this.Answer = designer.Reserve;
            }
        }

        private string _answer;
        public string Answer
        {
            get
            {
                return _answer;
            }
            set
            {
                SetProperty(ref _answer, value);
            }
        }
    }
}
