﻿using System;
using System.Collections.Generic;
using System.Text;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner.Demo.ViewModels
{
    public class CustomDefinedGroupDesignerItemViewModel : GroupDesignerItemViewModel
    {
        public CustomDefinedGroupDesignerItemViewModel() : this(null)
        {
        }

        public CustomDefinedGroupDesignerItemViewModel(IDiagramViewModel root) : base(root)
        {
        }

        public CustomDefinedGroupDesignerItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public CustomDefinedGroupDesignerItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        public override SelectableItemBase GetSerializableObject()
        {
            return new DesignerItemBase(this, Describe);
        }

        protected override void LoadDesignerItemViewModel(SelectableItemBase designerbase)
        {
            base.LoadDesignerItemViewModel(designerbase);

            if (designerbase is DesignerItemBase designer)
            {
                this.Describe = designer.Reserve;
            }
        }

        private string _describe;
        public string Describe
        {
            get
            {
                return _describe;
            }
            set
            {
                SetProperty(ref _describe, value);
            }
        }
    }
}
