﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AIStudio.Wpf.DiagramDesigner
{
    [Serializable]
    [XmlInclude(typeof(BlockDesignerItem))]
    public class BlockDesignerItem : DesignerItemBase
    {
        public BlockDesignerItem()
        {
        }

        public BlockDesignerItem(BlockDesignerItemViewModel viewmodel) : base(viewmodel)
        {
            Containers = new List<BlockItemsContainerInfoItem>(viewmodel.Containers.Select(p => new BlockItemsContainerInfoItem(p)));
            Flag = viewmodel.Flag;
        }


        [XmlArray]
        public List<BlockItemsContainerInfoItem> Containers
        {
            get; set;
        }

        [XmlAttribute]
        public string Flag
        {
            get; set;
        }
    }
}
