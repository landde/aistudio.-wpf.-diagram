﻿using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Models;
using System;
using System.Windows.Media;

namespace AIStudio.Wpf.DiagramDesigner
{
    /// <summary>
    /// DefaultTextNode
    /// </summary>
    public class TextDesignerItemViewModel : DesignerItemViewModelBase
    {
        public TextDesignerItemViewModel() : this(null)
        {

        }

        public TextDesignerItemViewModel(IDiagramViewModel root) : base(root)
        {

        }

        public TextDesignerItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public TextDesignerItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        public override SelectableItemBase GetSerializableObject()
        {
            return new TextDesignerItem(this);
        }

        public override void Init(IDiagramViewModel root, bool initNew)
        {
            base.Init(root, initNew);

            CustomText = true;   
        }

        protected override void InitNew()
        {
            this.ItemWidth = 150;
            this.InitIsEditing = true;
        }

        private string _watermark = "请输入文本";
        public string Watermark
        {
            get
            {
                return _watermark;
            }
            set
            {
                SetProperty(ref _watermark, value);
            }
        }

        protected override void ClearText()
        {
            Root.DeleteCommand.Execute(this);
        }
    }
}
