﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class TextAutoDesignerItemViewModel : TextDesignerItemViewModel
    {
        public TextAutoDesignerItemViewModel()
        {
        }

        public TextAutoDesignerItemViewModel(IDiagramViewModel root) : base(root)
        {
        }

        public TextAutoDesignerItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {
        }

        public TextAutoDesignerItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {
        }

        public override void Init(IDiagramViewModel root, bool initNew)
        {
            base.Init(root, initNew);

            CustomText = true;
        }

        protected override void InitNew()
        {
            base.InitNew();
            this.ItemWidth = double.NaN;
            this.ItemHeight = double.NaN;
            this.MinItemWidth = this.MinItemHeight = FontViewModel.FontSize * 2;
        }
    }
}
