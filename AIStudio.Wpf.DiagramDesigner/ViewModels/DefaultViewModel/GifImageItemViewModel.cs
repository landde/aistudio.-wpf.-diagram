﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class GifImageItemViewModel : DesignerItemViewModelBase
    {
        //private IDisposable propertyChangedSubscription;
        //private IDisposable connectorsChangedSubscription;

        public GifImageItemViewModel() : this(null)
        {

        }
        public GifImageItemViewModel(IDiagramViewModel root) : base(root)
        {

        }

        public GifImageItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public GifImageItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        public override SelectableItemBase GetSerializableObject()
        {
            return new MediaDesignerItem(this);
        }

        public override void Init(IDiagramViewModel root, bool initNew)
        {
            base.Init(root, initNew);

            //propertyChangedSubscription = WhenPropertyChanged.Where(o => o.ToString() == "Left" || o.ToString() == "Top" || o.ToString() == "ItemWidth" || o.ToString() == "ItemHeight").Subscribe(ChangeImageElement);
            //connectorsChangedSubscription = WhenConnectorsChanged.Subscribe(OnConnectorsChanged);
           
            AddCommand = new SimpleCommand(Command_Enable, ExecuteAddItemCommand);
            ImageSwitchCommand = new SimpleCommand(Command_Enable, ExecuteImageSwitchCommand);
            BuildMenuOptions();       
        }

        protected override void InitNew()
        {
            this.ClearConnectors();
        }

        protected override void LoadDesignerItemViewModel(SelectableItemBase designerbase)
        {
            base.LoadDesignerItemViewModel(designerbase);

            if (designerbase is MediaDesignerItem designer)
            {
                this.Icon = designer.Icon;             
            }
        }

        private bool _shouldInsertAnchor;
        public bool ShouldInsertAnchor
        {
            get
            {
                return _shouldInsertAnchor;
            }
            set
            {
                SetProperty(ref _shouldInsertAnchor, value);
            }
        }

        public ICommand AddCommand
        {
            get; private set;
        }

        public ICommand ImageSwitchCommand
        {
            get; private set;
        }

        private string dir = System.AppDomain.CurrentDomain.BaseDirectory + "Images\\Gifs";
        private void BuildMenuOptions()
        {
            if (Directory.Exists(dir))
            {
                menuOptions = new ObservableCollection<CinchMenuItem>();
                var equipmentImages = Directory.GetFiles(dir).Select(Path.GetFileName);
                foreach (var item in equipmentImages)
                {
                    CinchMenuItem menuItem = new CinchMenuItem();
                    menuItem.Text = item;
                    menuItem.Command = ImageSwitchCommand;
                    menuItem.CommandParameter = item;
                    menuOptions.Add(menuItem);
                }
            }
        }

        private void ExecuteAddItemCommand(object parameter)
        {
            FullyCreatedConnectorInfo connector = new FullyCreatedConnectorInfo(this, ConnectorOrientation.None, true);
            MouseButtonEventArgs mosueArg = ((EventToCommandArgs)parameter).EventArgs as MouseButtonEventArgs;
            var position = mosueArg.GetPosition(((EventToCommandArgs)parameter).Sender as IInputElement);
            connector.XRatio = (position.X - connector.ConnectorWidth / 2) / connector.DataItem.GetItemWidth();
            connector.YRatio = (position.Y - connector.ConnectorHeight / 2) / connector.DataItem.GetItemHeight();
            AddConnector(connector);
        }

        private void ExecuteImageSwitchCommand(object parameter)
        {
            string image = parameter as string;
            string path = dir + @"\{0}";
            string filePath = string.Format(path, image);
            if (File.Exists(filePath))
            {
                Icon = filePath;
            }
        }

        //private void OnConnectorsChanged(NotifyCollectionChangedEventArgs args)
        //{
        //    if (args.Action == NotifyCollectionChangedAction.Add)
        //    {
        //        if (args.NewItems.Count > 0)
        //        {
        //            foreach (var item in args.NewItems)
        //            {

        //            }
        //        }
        //    }
        //    else if (args.Action == NotifyCollectionChangedAction.Delete)
        //    {
        //        if (args.OldItems.Count > 0)
        //        {
        //            foreach (var item in args.OldItems)
        //            {

        //            }
        //        }
        //    }
        //    else if (args.Action == NotifyCollectionChangedAction.Reset)
        //    {

        //    }
        //}

    }
}
