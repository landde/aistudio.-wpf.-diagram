﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class LineDrawingDesignerItemViewModel : DrawingDesignerItemViewModelBase
    {
        public LineDrawingDesignerItemViewModel()
        {
        }

        public LineDrawingDesignerItemViewModel(IDiagramViewModel root, Point startPoint, ColorViewModel colorViewModel, bool erasable) : base(root, DrawMode.ErasableLine, startPoint, colorViewModel, erasable)
        {
        }

        public LineDrawingDesignerItemViewModel(IDiagramViewModel root, List<Point> points, ColorViewModel colorViewModel, bool erasable) : base(root, DrawMode.ErasableLine, points, colorViewModel, erasable)
        {

        }

        public LineDrawingDesignerItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public LineDrawingDesignerItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        protected override void InitNewDrawing()
        {
            if (IsFinish)
            {
                Geometry = new LineGeometry(Points[0], Points[1]);
            }
            base.InitNewDrawing();
        }

        public override bool OnMouseMove(IInputElement sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var point = e.GetPosition(sender);
                if (Points == null || Points.Count == 0)//没有起始点
                {
                    return true;
                }

                if ((Points[0] - point).Length < ColorViewModel.LineWidth)
                {
                    return true;
                }

                if (Points.Count == 2)
                {
                    Points[1] = point;
                }
                else
                {
                    Points.Add(point);
                }

                Geometry = new LineGeometry(Points[0], Points[1]);
                IsFinish = true;

                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool OnMouseDown(IInputElement sender, MouseButtonEventArgs e)
        {
            return true;
        }

        public override bool OnMouseUp(IInputElement sender, MouseButtonEventArgs e)
        {
            return base.OnMouseUp(sender, e);
        }
    }
}
