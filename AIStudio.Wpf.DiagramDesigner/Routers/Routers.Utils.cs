﻿using AIStudio.Wpf.DiagramDesigner.Geometrys;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner
{
    public static partial class Routers
    {
        public static PointBase GetPortPositionBasedOnAlignment(ConnectorInfoBase port)
        {
            var pt = port.Position;
            switch (port.Orientation)
            {
                case ConnectorOrientation.Left:
                    return new PointBase(pt.X, pt.Y + port.ConnectorHeight / 2);
                case ConnectorOrientation.TopLeft:
                    return new PointBase(pt.X, pt.Y);
                case ConnectorOrientation.Top:
                    return new PointBase(pt.X + port.ConnectorWidth / 2, pt.Y);
                case ConnectorOrientation.TopRight:
                    return new PointBase(pt.X + port.ConnectorWidth, pt.Y);
                case ConnectorOrientation.Right:
                    return new PointBase(pt.X + port.ConnectorWidth, pt.Y + port.ConnectorHeight / 2);
                case ConnectorOrientation.BottomRight:
                    return new PointBase(pt.X + port.ConnectorWidth, pt.Y + port.ConnectorHeight);
                case ConnectorOrientation.Bottom:
                    return new PointBase(pt.X + port.ConnectorWidth / 2, pt.Y + port.ConnectorHeight);
                case ConnectorOrientation.BottomLeft:
                    return new PointBase(pt.X, pt.Y + port.ConnectorHeight);    
                default:
                    return pt;
            }
        }
    }
}
