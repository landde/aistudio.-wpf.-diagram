﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using AIStudio.Wpf.DiagramDesigner.Geometrys;

// Implementation taken from the JS version: https://gist.github.com/menendezpoo/4a8894c152383b9d7a870c24a04447e4
// Todo: Make it more c#, Benchmark A* vs Dijkstra, Add more options
namespace AIStudio.Wpf.DiagramDesigner
{
    public static partial class Routers
    {
        public static PointBase[] FishBone(IDiagramViewModel _, ConnectionViewModel link)
        {
            if (link.IsPortless)
                throw new Exception("Orthogonal router doesn't work with portless links yet");

            if (link.IsFullConnection == false)
                return Normal(_, link);

            double x2 = link.SourceConnectorInfoFully.MiddlePosition.X < link.SinkConnectorInfoFully.MiddlePosition.X ?
                link.SinkConnectorInfoFully.MiddlePosition.X - Math.Abs(link.SourceConnectorInfoFully.MiddlePosition.Y - link.SinkConnectorInfoFully.MiddlePosition.Y)
                : link.SinkConnectorInfoFully.MiddlePosition.X + Math.Abs(link.SourceConnectorInfoFully.MiddlePosition.Y - link.SinkConnectorInfoFully.MiddlePosition.Y);
            double y2 = link.SourceConnectorInfoFully.MiddlePosition.Y;

            return new PointBase[] { new PointBase(x2, y2) };

        }

    }


}
