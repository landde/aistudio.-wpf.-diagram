﻿using AIStudio.Wpf.Logical.ViewModels;
using AIStudio.Wpf.DiagramDesigner;
using System.Xml.Serialization;
using System;

namespace AIStudio.Wpf.Logical.Models
{
    [Serializable]
    [XmlInclude(typeof(LogicalGateItem))]
    public class LogicalGateItem : LogicalGateDesignerItemBase
    {
        public LogicalGateItem()
        {

        }
        public LogicalGateItem(LogicalGateItemViewModel item) : base(item)
        {

        }     



    }
}
