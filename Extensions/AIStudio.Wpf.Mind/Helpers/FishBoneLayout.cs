﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Algorithms;
using AIStudio.Wpf.DiagramDesigner.Geometrys;
using AIStudio.Wpf.DiagramDesigner.Helpers;
using AIStudio.Wpf.Mind.Models;
using AIStudio.Wpf.Mind.ViewModels;

namespace AIStudio.Wpf.Mind.Helpers
{
    public class FishBoneLayout : IMindLayout
    {
        public void Appearance(MindNode mindNode)
        {
            Appearance(mindNode, MindTheme.SkyBlue, false);
        }

        public void Appearance(MindNode mindNode, MindTheme mindTheme, bool initAppearance)
        {
            if (mindNode == null) return;

            mindNode.GetLevel0Node().LayoutUpdating = true;

            switch (mindNode.NodeLevel)
            {
                case 0:
                    {
                        if (initAppearance)
                        {
                            MindThemeHelper.ThemeChange(mindNode, mindTheme, initAppearance);

                            mindNode.ClearConnectors();
                            var port = new FullyCreatedConnectorInfo(mindNode.Root, mindNode, ConnectorOrientation.Right, true) { XRatio = 1, YRatio = 0.5 };
                            mindNode.AddConnector(port);
                        }
                        mindNode.ShapeViewModel.SinkMarker.PathStyle = PathStyle.None;
                        mindNode.ShapeViewModel.SinkMarker.SizeStyle = SizeStyle.VerySmall;
                        mindNode.ConnectorOrientation = ConnectorOrientation.None;
                        break;
                    }
                case 1:
                    {
                        if (initAppearance)
                        {
                            MindThemeHelper.ThemeChange(mindNode, mindTheme, initAppearance);

                            mindNode.ClearConnectors();
                            var port1 = new FullyCreatedConnectorInfo(mindNode.Root, mindNode, ConnectorOrientation.BottomLeft, true) { XRatio = 0, YRatio = 1 };
                            mindNode.AddConnector(port1);
                            var port2 = new FullyCreatedConnectorInfo(mindNode.Root, mindNode, ConnectorOrientation.TopLeft, true) { XRatio = 0, YRatio = 0 };
                            mindNode.AddConnector(port2);
                        }

                        mindNode.ShapeViewModel.SinkMarker.PathStyle = PathStyle.None;
                        mindNode.ShapeViewModel.SinkMarker.SizeStyle = SizeStyle.VerySmall;
                        mindNode.ConnectorOrientation = ConnectorOrientation.Left; 
                        break;
                    }
                default:
                    {
                        if (initAppearance)
                        {   
                            MindThemeHelper.ThemeChange(mindNode, mindTheme, initAppearance);

                            mindNode.ClearConnectors();
                            var port1 = new FullyCreatedConnectorInfo(mindNode.Root, mindNode, ConnectorOrientation.Left, true) { XRatio = 0, YRatio = 0.5 };
                            mindNode.AddConnector(port1);
                            var port2 = new FullyCreatedConnectorInfo(mindNode.Root, mindNode, ConnectorOrientation.Bottom, true) { XRatio = 0.25, YRatio = 1 };
                            mindNode.AddConnector(port2);
                            var port3 = new FullyCreatedConnectorInfo(mindNode.Root, mindNode, ConnectorOrientation.Top, true) { XRatio = 0.25, YRatio = 0 };
                            mindNode.AddConnector(port3);

                            mindNode.CornerRadius = new System.Windows.CornerRadius(0);
                            mindNode.BorderThickness = new System.Windows.Thickness(0, 0, 0, 0);                          
                        }
                        mindNode.ShapeViewModel.SinkMarker.PathStyle = PathStyle.None;
                        mindNode.ShapeViewModel.SinkMarker.SizeStyle = SizeStyle.VerySmall;
                        mindNode.ConnectorOrientation = ConnectorOrientation.Left;
                        break;
                    }
            }

            mindNode.GetLevel0Node().LayoutUpdating = false;
        }

        public ConnectionViewModel GetOrSetConnectionViewModel(MindNode source, MindNode sink, ConnectionViewModel connector = null)
        {
            if (source == null || sink == null)
                return null;

            DrawMode drawMode;
            RouterMode routerMode;
            if (source.NodeLevel == 0)
            {
                drawMode = DrawMode.ConnectingLineStraight;
                routerMode = RouterMode.RouterFishBone;
            }
            else if (source.NodeLevel == 1)
            {
                drawMode = DrawMode.ConnectingLineStraight;
                routerMode = RouterMode.RouterNormal;
            }
            else
            {
                drawMode = DrawMode.ConnectingLineStraight;
                routerMode = RouterMode.RouterOrthogonal;
            }

            if (connector == null)
            {
                connector = new ConnectionViewModel(source.Root, source.FirstConnector, sink.FirstConnector, drawMode, routerMode);
            }
            else
            {
                connector?.UpdateConnectionMode(source.FirstConnector, sink.FirstConnector, drawMode.ToString(), routerMode.ToString());
            }
            connector.EnabledForSelection = false;
            connector.IsHitTestVisible = false;
            connector.ColorViewModel.LineColor = source.ColorViewModel.LineColor;
            connector.ShapeViewModel.SinkMarker.PathStyle = source.ShapeViewModel.SinkMarker.PathStyle;
            connector.ShapeViewModel.SinkMarker.SizeStyle = source.ShapeViewModel.SinkMarker.SizeStyle;
            connector.SetPathGeneratorParameter(smoothMargin: 20, smoothAutoSlope: 0.2, orthogonalShapeMargin: 2, orthogonalGlobalBoundsMargin: 5);

            return connector;
        }

        public void UpdatedLayout(MindNode mindNode)
        {
            if (mindNode == null) return;

            mindNode.GetLevel0Node().LayoutUpdating = true;
            var size = MeasureOverride(mindNode);
            ArrangeOverride(mindNode);

            mindNode.Root.BringToFrontCommand.Execute(mindNode.Root.Items.OfType<DesignerItemViewModelBase>());

            mindNode.GetLevel0Node().LayoutUpdating = false;
        }

        public SizeBase MeasureOverride(MindNode mindNode, bool isExpanded = true)
        {
            var sizewithSpacing = mindNode.SizeWithSpacing;
            var bottomoffset = mindNode.Spacing.Width / 2;
            if (mindNode.Children?.Count > 0)
            {
                if (mindNode.NodeLevel == 0)
                {
                    var tops = mindNode.Children.Where((p, index) => index % 2 == 0).ToList();
                    tops.ForEach(p => p.ConnectorOrientation = ConnectorOrientation.BottomLeft);
                    var topsizes = tops.Select(p => MeasureOverride(p, mindNode.IsExpanded && isExpanded)).ToArray();
                    var bottoms = mindNode.Children.Where((p, index) => index % 2 == 1).ToList();
                    bottoms.ForEach(p => p.ConnectorOrientation = ConnectorOrientation.TopLeft);
                    var bottomsizes = bottoms.Select(p => MeasureOverride(p, mindNode.IsExpanded && isExpanded)).ToArray();
                    sizewithSpacing = new SizeBase(sizewithSpacing.Width + bottomoffset + Math.Max(topsizes.SumOrDefault(p => p.Width), bottomsizes.SumOrDefault(p => p.Width)), sizewithSpacing.Height + topsizes.MaxOrDefault(p => p.Height) + bottomsizes.MaxOrDefault(p => p.Height));
                }
                else if (mindNode.NodeLevel == 1)
                {
                    var childrensizes = mindNode.Children.Select(p => MeasureOverride(p, mindNode.IsExpanded && isExpanded)).ToArray();
                    var lastchildsize = childrensizes.LastOrDefault();
                    sizewithSpacing = new SizeBase(Math.Max(sizewithSpacing.Width, sizewithSpacing.Height + childrensizes.SumOrDefault(p => p.Height) - lastchildsize.Height / 2 + lastchildsize.Width), sizewithSpacing.Height + childrensizes.SumOrDefault(p => p.Height));
                }
                else
                {
                    var childrensizes = mindNode.Children.Select(p => MeasureOverride(p, mindNode.IsExpanded && isExpanded)).ToArray();
                    sizewithSpacing = new SizeBase(Math.Max(sizewithSpacing.Width, sizewithSpacing.Width * 0.5 + childrensizes.MaxOrDefault(p => p.Width)), sizewithSpacing.Height + childrensizes.SumOrDefault(p => p.Height));
                }
            }
            mindNode.DesiredSize = isExpanded ? sizewithSpacing : new SizeBase(0, 0);
            mindNode.Visible = isExpanded;

            return mindNode.DesiredSize;
        }

        public void ArrangeOverride(MindNode mindNode)
        {
            if (mindNode.NodeLevel == 0)
            {
                mindNode.DesiredPosition = mindNode.Position;

                var tops = mindNode.Children.Where(p => p.ConnectorOrientation == ConnectorOrientation.BottomLeft).ToList();
                double topleft = mindNode.DesiredPosition.X + mindNode.ItemWidth  + mindNode.Spacing.Width;
                double toptop = mindNode.DesiredPosition.Y - mindNode.Spacing.Height;

                if (mindNode.Children?.Count > 0)
                {
                    foreach (var child in tops)
                    {
                        child.Offset = new PointBase(child.Offset.X - child.RootNode.Offset.X, child.Offset.Y - child.RootNode.Offset.Y);//按根节点修正Offset
                        child.DesiredPosition = new PointBase(topleft + child.Spacing.Width, toptop - child.ItemHeight - child.Spacing.Height);
                        child.Left = child.DesiredPosition.X + child.Offset.X;
                        child.Top = child.DesiredPosition.Y + child.Offset.Y;
                        topleft += child.DesiredSize.Width;

                        ArrangeOverride(child);

                        var connector = mindNode.Root?.Items.OfType<ConnectionViewModel>().FirstOrDefault(p => p.SourceConnectorInfoFully?.DataItem == mindNode && p.SinkConnectorInfoFully?.DataItem == child);
                        connector?.SetSourcePort(mindNode.RightConnector);
                        connector?.SetSinkPort(child.BottomLeftConnector);
                        connector?.SetVisible(child.Visible);
                    }
                }

                var bottomoffset = mindNode.Spacing.Width / 2;
                var bottoms = mindNode.Children.Where(p => p.ConnectorOrientation == ConnectorOrientation.TopLeft).ToList();
                double bottomleft = mindNode.DesiredPosition.X + mindNode.ItemWidth + mindNode.Spacing.Width + bottomoffset;
                double bottomtop = mindNode.DesiredPosition.Y + mindNode.ItemHeight + mindNode.Spacing.Height;

                if (mindNode.Children?.Count > 0)
                {
                    foreach (var child in bottoms)
                    {
                        child.Offset = new PointBase(child.Offset.X - child.RootNode.Offset.X, child.Offset.Y - child.RootNode.Offset.Y);//按根节点修正Offset
                        child.DesiredPosition = new PointBase(bottomleft + child.Spacing.Width, bottomtop + child.Spacing.Height);
                        child.Left = child.DesiredPosition.X + child.Offset.X;
                        child.Top = child.DesiredPosition.Y + child.Offset.Y;
                        bottomleft += child.DesiredSize.Width;

                        ArrangeOverride(child);

                        var connector = mindNode.Root?.Items.OfType<ConnectionViewModel>().FirstOrDefault(p => p.SourceConnectorInfoFully?.DataItem == mindNode && p.SinkConnectorInfoFully?.DataItem == child);
                        connector?.SetSourcePort(mindNode.RightConnector);
                        connector?.SetSinkPort(child.TopLeftConnector);
                        connector?.SetVisible(child.Visible);
                    }
                }
            
                mindNode.Offset = new PointBase();//修正后归0
            }
            else if (mindNode.NodeLevel == 1)
            {
                if (mindNode.ConnectorOrientation == ConnectorOrientation.BottomLeft)
                {
                    double x0 = mindNode.DesiredPosition.X;
                    double y0 = mindNode.DesiredPosition.Y + mindNode.ItemHeight;
                    double h = mindNode.ItemHeight + mindNode.Spacing.Height;
                    if (mindNode.Children?.Count > 0)
                    {
                        foreach (var child in mindNode.Children)
                        {
                            child.Offset = new PointBase(child.Offset.X - child.RootNode.Offset.X, child.Offset.Y - child.RootNode.Offset.Y);//按根节点修正Offset
                            child.DesiredPosition = new PointBase(x0 + (h + child.DesiredSize.Height - child.ItemHeight / 2 - child.Spacing.Height), y0 - (h + child.DesiredSize.Height - child.Spacing.Height));
                            child.Left = child.DesiredPosition.X + child.Offset.X;
                            child.Top = child.DesiredPosition.Y + child.Offset.Y;
                            h += child.DesiredSize.Height;

                            ArrangeOverride(child);

                            var connector = mindNode.Root?.Items.OfType<ConnectionViewModel>().FirstOrDefault(p => p.SourceConnectorInfoFully?.DataItem == mindNode && p.SinkConnectorInfoFully?.DataItem == child);
                            connector?.SetSourcePort(mindNode.BottomLeftConnector);
                            connector?.SetSinkPort(child.LeftConnector);
                            connector?.SetVisible(child.Visible);
                        }
                    }
                }
                else
                {
                    double x0 = mindNode.DesiredPosition.X;
                    double y0 = mindNode.DesiredPosition.Y;
                    double h = mindNode.ItemHeight + mindNode.Spacing.Height;
                    if (mindNode.Children?.Count > 0)
                    {
                        foreach (var child in mindNode.Children)
                        {
                            child.Offset = new PointBase(child.Offset.X - child.RootNode.Offset.X, child.Offset.Y - child.RootNode.Offset.Y);//按根节点修正Offset                    
                            child.DesiredPosition = new PointBase(x0 + (h + child.DesiredSize.Height - child.ItemHeight / 2 - child.Spacing.Height), y0 + (h + child.DesiredSize.Height - child.ItemHeight - child.Spacing.Height));
                            child.Left = child.DesiredPosition.X + child.Offset.X;
                            child.Top = child.DesiredPosition.Y + child.Offset.Y;
                            h += child.DesiredSize.Height;

                            ArrangeOverride(child);

                            var connector = mindNode.Root?.Items.OfType<ConnectionViewModel>().FirstOrDefault(p => p.SourceConnectorInfoFully?.DataItem == mindNode && p.SinkConnectorInfoFully?.DataItem == child);
                            connector?.SetSourcePort(mindNode.TopLeftConnector);
                            connector?.SetSinkPort(child.LeftConnector);
                            connector?.SetVisible(child.Visible);
                        }
                    }
                }
            }
            else
            {
                if (mindNode.GetLevel1Node().ConnectorOrientation == ConnectorOrientation.BottomLeft)
                {
                    double left = mindNode.DesiredPosition.X + mindNode.ItemWidth / 2;
                    double top = mindNode.DesiredPosition.Y + mindNode.ItemHeight + mindNode.Spacing.Height;
                    if (mindNode.Children?.Count > 0)
                    {
                        foreach (var child in mindNode.Children)
                        {
                            child.Offset = new PointBase(child.Offset.X - child.RootNode.Offset.X, child.Offset.Y - child.RootNode.Offset.Y);//按根节点修正Offset
                            child.DesiredPosition = new PointBase(left, top + child.Spacing.Height);
                            child.Left = child.DesiredPosition.X + child.Offset.X;
                            child.Top = child.DesiredPosition.Y + child.Offset.Y;
                            top += child.DesiredSize.Height;

                            ArrangeOverride(child);

                            var connector = mindNode.Root?.Items.OfType<ConnectionViewModel>().FirstOrDefault(p => p.SourceConnectorInfoFully?.DataItem == mindNode && p.SinkConnectorInfoFully?.DataItem == child);
                            connector?.SetSourcePort(mindNode.BottomConnector);
                            connector?.SetSinkPort(child.LeftConnector);
                            connector?.SetVisible(child.Visible);
                        }
                    }
                }
                else
                {
                    double left = mindNode.DesiredPosition.X + mindNode.ItemWidth / 2;
                    double bottom = mindNode.DesiredPosition.Y - mindNode.Spacing.Height;
                    if (mindNode.Children?.Count > 0)
                    {
                        foreach (var child in mindNode.Children)
                        {
                            child.Offset = new PointBase(child.Offset.X - child.RootNode.Offset.X, child.Offset.Y - child.RootNode.Offset.Y);//按根节点修正Offset
                            child.DesiredPosition = new PointBase(left, bottom - child.Spacing.Height - child.ItemHeight);
                            child.Left = child.DesiredPosition.X + child.Offset.X;
                            child.Top = child.DesiredPosition.Y + child.Offset.Y;
                            bottom -= child.DesiredSize.Height;

                            ArrangeOverride(child);

                            var connector = mindNode.Root?.Items.OfType<ConnectionViewModel>().FirstOrDefault(p => p.SourceConnectorInfoFully?.DataItem == mindNode && p.SinkConnectorInfoFully?.DataItem == child);
                            connector?.SetSourcePort(mindNode.TopConnector);
                            connector?.SetSinkPort(child.LeftConnector);
                            connector?.SetVisible(child.Visible);
                        }
                    }
                }
            }
        }
    }

}
