﻿using AIStudio.Wpf.Flowchart;
using AIStudio.Wpf.SFC;
using System;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Helpers;
using System.Windows;
using System.Collections.Generic;
using AIStudio.Wpf.DiagramDesigner.Models;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace AIStudio.Wpf.DiagramApp.Models
{
    public class PathToolBoxData : ToolBoxData
    {
        public PathToolBoxData(string icon, Type type, double width = 32, double height = 32, Size? desiredSize = null, Size? desiredMinSize = null, string description = null) : base(null, icon, type, width, height, desiredSize, desiredMinSize, description)
        {
            ColorViewModel.FillColor.Color = Colors.Orange;
        }

    }

    public class TextToolBoxData : ToolBoxData
    {
        public TextToolBoxData(string text, Type type, double width = 32, double height = 32, Size? desiredSize = null, Size? desiredMinSize = null, string description = null) : base(text, null, type, width, height, desiredSize, desiredMinSize, description)
        {
            ColorViewModel.FillColor.Color = Colors.Orange;
        }

    }

    public class EllipseTextToolBoxData : ToolBoxData
    {
        public EllipseTextToolBoxData(string text, Type type, double width = 32, double height = 32, Size? desiredSize = null, Size? desiredMinSize = null, string description = null) : base(text, null, type, width, height, desiredSize, desiredMinSize, description)
        {
            ColorViewModel.FillColor.Color = Colors.Orange;
        }

    }

    public class ImageToolBoxData : ToolBoxData
    {
        public ImageToolBoxData(string icon, Type type, double width = 32, double height = 32, Size? desiredSize = null, Size? desiredMinSize = null, string description = null) : base(null, icon, type, width, height, desiredSize, desiredMinSize, description)
        {

        }
    }

    public class DesignerItemToolBoxData : ToolBoxData
    {
        public string FileName
        {
            get; set;
        }
        public DesignerItemViewModelBase DesignerItemViewModel
        {
            get; set;
        }
        public DesignerItemToolBoxData(DesignerItemBase designerItemBase, string filename, Type type, double width = 32, double height = 32, Size? desiredSize = null, Size? desiredMinSize = null, string description = null) : base(null, null, type, width, height, desiredSize, desiredMinSize, description)
        {
            Addition = designerItemBase;
            DesignerItemViewModel = Activator.CreateInstance(type, null, designerItemBase) as DesignerItemViewModelBase;
            FileName = filename;

        }
    }

    public class MultipleDesignerItemToolBoxData : ToolBoxData
    {
        public string FileName
        {
            get; set;
        }
        public List<SelectableDesignerItemViewModelBase> SelectableDesignerItemViewModels
        {
            get; set;
        }
        public double CanvasWidth
        {
            get; set;
        }
        public double CanvasHeight
        {
            get; set;
        }
        public MultipleDesignerItemToolBoxData(SerializableObject designerItemBase, string filename, double width = 32, double height = 32, Size? desiredSize = null, Size? desiredMinSize = null, string description = null) : base(null, null, typeof(List<SelectableDesignerItemViewModelBase>), width, height, desiredSize, desiredMinSize, description)
        {
            SelectableDesignerItemViewModels = designerItemBase.ToObject();

            var minleft = SelectableDesignerItemViewModels.OfType<DesignerItemViewModelBase>().Min(p => p.Left);
            var mintop = SelectableDesignerItemViewModels.OfType<DesignerItemViewModelBase>().Min(p => p.Top);
            var maxright = SelectableDesignerItemViewModels.OfType<DesignerItemViewModelBase>().Max(p => p.Left + p.ItemWidth);
            var maxbottom = SelectableDesignerItemViewModels.OfType<DesignerItemViewModelBase>().Max(p => p.Top + p.ItemHeight);

            foreach (var item in SelectableDesignerItemViewModels.OfType<DesignerItemViewModelBase>())
            {
                item.Left -= minleft;
                item.Top -= mintop;
            }
            CanvasWidth = maxright - minleft;
            CanvasHeight = maxbottom - mintop;

            designerItemBase.DesignerItems = SelectableDesignerItemViewModels.OfType<DesignerItemViewModelBase>().Select(p => p.ToSerializableItem(".json")).Where(p => p != null).ToList();
            designerItemBase.Connections = SelectableDesignerItemViewModels.OfType<ConnectionViewModel>().Select(p => p.ToSerializableItem(".json")).Where(p => p != null).ToList();

            Addition = designerItemBase;
            FileName = filename;
        }
    }

    public class SvgToolBoxData : ToolBoxData
    {
        public SvgToolBoxData(string filename, Type type, double width = 32, double height = 32, Size? desiredSize = null, Size? desiredMinSize = null, string description = null) : base(null, filename, type, width, height, desiredSize, desiredMinSize, description)
        {
            ColorViewModel.FillColor.Color = Colors.Blue;
        }
    }

    public class MediaToolBoxData : ToolBoxData
    {
        public MediaToolBoxData(string icon, Type type, double width = 32, double height = 32, Size? desiredSize = null, Size? desiredMinSize = null, string description = null) : base(icon, null, type, width, height, desiredSize, desiredMinSize, description)
        {

        }
    }


}
